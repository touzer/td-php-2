<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Configuration\Configuration;
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class VerificationEmail
{
    public static function envoiEmailValidation(Utilisateur $utilisateur): void
    {
        $loginURL = rawurlencode($utilisateur->getLogin());
        $nonceURL = rawurlencode($utilisateur->getNonce());
        $absoluteURL = Configuration::getAbsoluteURL();
        $lienValidationEmail = "$absoluteURL?action=validerEmail&controller=utilisateur&login=$loginURL&nonce=$nonceURL";
        $corpsEmail = "<a href=\"$lienValidationEmail\">Validation</a>";

        // Temporairement avant d'envoyer un vrai mail
        MessageFlash::ajouter("success", $corpsEmail);
    }

    public static function traiterEmailValidation($login, $nonce): bool
    {
        // À compléter
        $user =(new UtilisateurRepository())->recupererParClePrimaire($login);
        if (!is_null($user)){
            if ($user->formatTableau()["nonce"]==$nonce){
                $user->setEmail($user->getEmailAValider());
                $user->setEmailAValider("");
                $user->setNonce("");
                (new UtilisateurRepository())->mettreAJour($user);
                return true;
            }
        }
        return false;
    }

    public static function aValideEmail(Utilisateur $utilisateur) : bool
    {
        // À compléter
        if ($utilisateur->getEmail()!="") return true;
        return false;
    }
}