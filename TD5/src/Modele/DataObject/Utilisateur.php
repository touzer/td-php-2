<?php

namespace App\Covoiturage\Modele\DataObject;


use App\Covoiturage\Lib\MotDePasse;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    private string $mdpHache;

    private bool $estAdmin;

    private string $email;
    private string $emailAValider;
    private string $nonce;

    public function __construct(string $login, string $nom, string $prenom,string $mdpHache,bool $estAdmin,string $email,string $emailAValider,string $nonce)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache=$mdpHache;
        $this->estAdmin=$estAdmin;
        $this->email=$email;
        $this->emailAValider=$emailAValider;
        $this->nonce=$nonce;
    }


    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache($mdpClair):string{
        return hash('sha256',$mdpClair);
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }



    public function formatTableau(): array
    {
        if ($this->estAdmin){
            $bool=1;
        }else $bool=0;
        return ["login"=>$this->login,
            "nom" =>$this->nom,
            "prenom"=>$this->prenom,
            "mdpHache"=>$this->mdpHache,
            "estAdmin"=>$bool,
            "email"=>$this->email,
            "emailAValider"=>$this->emailAValider,
            "nonce"=>$this->nonce
        ];
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }



    public static function construireDepuisFormulaire (array $tableauFormulaire) : Utilisateur
    {

        $mdpHache = hash("sha256", $tableauFormulaire["mdp"]);
        if (isset($tableauFormulaire["estAdmin"]) && $tableauFormulaire["estAdmin"]=="on"){
            $bool=true;
        }else $bool=false;
        return new Utilisateur(
            $tableauFormulaire["login"],
            $tableauFormulaire["nom"],
            $tableauFormulaire["prenom"],
            $mdpHache,
            $bool,
            "",
            $tableauFormulaire["email"],
            MotDePasse::genererChaineAleatoire()
        );
    }



}