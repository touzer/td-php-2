<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class Voiture extends AbstractDataObject
{

    private string $immatriculation;
    private string $marque;
    private string $couleur;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque(): string
    {
        return $this->marque;
    }

    public function getCouleur(): string
    {
        return $this->couleur;
    }

    public function getImmatriculation(): string
    {
        return $this->immatriculation;
    }

    public function getNbSieges(): int
    {
        return $this->nbSieges;
    }

    // un setter
    public function setMarque(string $marque)
    {
        $this->marque = $marque;
    }

    public function setCouleur(string $couleur)
    {
        $this->marque = $couleur;
    }

    public function setImmatriculation(string $immatriculation)
    {
        $this->marque = substr($immatriculation, 0, 8);
    }

    public function setNbSieges(string $nbSieges)
    {
        $this->marque = $nbSieges;
    }

    // un constructeur
    public function __construct(
        string $immatriculation,
        string $marque,
        string $couleur,
        int    $nbSieges
    )
    {
        $this->immatriculation = substr($immatriculation, 0, 8);
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->nbSieges = $nbSieges;
    }

    public function formatTableau(): array
    {
        return ["immatriculation"=>$this->getImmatriculation(),
            "marque"=>$this->marque,
            "couleur"=>$this->couleur,
            "nbSieges" =>$this->nbSieges];
    }

}

?>