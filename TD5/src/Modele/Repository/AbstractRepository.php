<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Voiture;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnee;
use App\Covoiturage\Modele\Repository\VoitureRepository;

abstract class AbstractRepository{

    public function sauvegarder(AbstractDataObject $object): bool
    {
        // on suppose que les attributs sont tous non nuls
        try {
            $sql = "INSERT INTO ".$this->getNomTable()." VALUES (";
            foreach ($this->getNomsColonnes() as $nomsColonne) {
                if ($nomsColonne!=$this->getNomsColonnes()[0]){
                    $sql.=",";
                }
                $sql.=":".$nomsColonne."Tag";
                $values[$nomsColonne."Tag"]=$object->formatTableau()[$nomsColonne];
            }
            $sql.=")";
            $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);
            $pdoStatement->execute($values);
        } catch (PDOException $e) {
            echo $e->getMessage() . "<br>";
            return false;
        }
        return true;

    }

    public function mettreAJour(AbstractDataObject $object): void
    {
        try {
            $sql = "UPDATE ".$this->getNomTable()." SET ";//, couleur= :couleur, nbSieges= :nbSieges WHERE immatriculation= :immat";
            foreach ($this->getNomsColonnes() as $nomsColonne) {
                if ($nomsColonne!=$this->getNomClePrimaire()) {
                    if ($nomsColonne!=$this->getNomsColonnes()[1]){
                        $sql.=",";
                    }
                    $sql .= $nomsColonne . "=:Tag" . $nomsColonne . " ";
                    $values["Tag" . $nomsColonne] = $object->formatTableau()[$nomsColonne];
                }
            }
            $sql.=" WHERE ".$this->getNomClePrimaire()." =:TagCle ";
            $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);
            $values["TagCle"]=$object->formatTableau()[$this->getNomClePrimaire()];
            $pdoStatement->execute($values);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public abstract function getNomsColonnes():array;

    public function supprimer(string $valeurClePrimaire): bool
    {
        try {
            $sql = "DELETE FROM ".$this->getNomTable() ." WHERE ".$this->getNomClePrimaire()."=:immatTag";
            $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);
            $values = [
                "immatTag" => $valeurClePrimaire,
            ];
            $pdoStatement->execute($values);
        } catch (\PDOException $e) {
            echo $e->getMessage() . "<br>";
            return false;
        }
        return true;
    }

    public function recuperer(): array
    {
        $sql="SELECT * FROM ".$this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->query($sql);
        $voitureTableau = array();
        foreach ($pdoStatement as $voitureFormatTableau) {
            $voitureTableau[] = $this->construireDepuisTableau($voitureFormatTableau);
        }
        return $voitureTableau;
    }
    protected abstract function getNomTable(): string;

    protected abstract function construireDepuisTableau(array $objetFormatTableau) : AbstractDataObject;

    public function recupererParClePrimaire(string $valeurClePrimaire): ?AbstractDataObject
    {

        $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voitureFormatTableau = $pdoStatement->fetch();
        if (!$voitureFormatTableau) {
            return null;
        }
        return $this->construireDepuisTableau($voitureFormatTableau);
    }

    protected abstract function getNomClePrimaire(): string;


}