<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\AbstractRepository;

class TrajetRepository extends AbstractRepository
{

    public function getNomsColonnes(): array
    {
        return ["id","depart","arrivee","date","nbPlaces","prix","conducteurLogin"];
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }
    public function construireDepuisTableau(array $objetFormatTableau): AbstractDataObject
    {
        return new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            $objetFormatTableau["date"],
            $objetFormatTableau["nbPlaces"],
            $objetFormatTableau["prix"],
            $objetFormatTableau["conducteurLogin"]

        );
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }
}