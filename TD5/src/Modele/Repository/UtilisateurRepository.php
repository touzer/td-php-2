<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnee;
class UtilisateurRepository extends AbstractRepository
{


    public function construireDepuisTableau(array $utilisateurTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"],
            $utilisateurTableau["mdpHache"],
            $utilisateurTableau["estAdmin"],
            $utilisateurTableau["email"],
            $utilisateurTableau["emailAValider"],
            $utilisateurTableau["nonce"]
        );
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    public function getNomsColonnes(): array
    {
        return ["login","nom","prenom","mdpHache","estAdmin","email","emailAValider","nonce"];
    }

    public function getTrajetsParConducteur(Utilisateur $conducteur) : ?array{
        $sql="SELECT * FROM trajet t WHERE conducteurLogin=:loginTag";
        $pdoStatement=ConnexionBaseDeDonnee::getPdo()->prepare($sql);
        $aray=array(
            "loginTag"=>$conducteur->getLogin()
        );
        $pdoStatement->execute($aray);
        $listeTrajets=array();
        foreach ($pdoStatement as $trajet) {
            $listeTrajets[]=$trajet['id'];
        }
        if (empty($listeTrajets)) return Null;
        return $listeTrajets;

    }

    public function estAdmin(string $login): bool{
        $sql="SELECT estAdmin FROM utilisateur WHERE login=:Tag";
        $pdoStatement=ConnexionBaseDeDonnee::getPdo()->prepare($sql);
        $array=array(
            "Tag"=>$login
        );
        $pdoStatement->execute($array);
        foreach ($pdoStatement as $item) {
            if ($item["estAdmin"]=="1"){
                return true;
            }
        }
        return false;
    }
}