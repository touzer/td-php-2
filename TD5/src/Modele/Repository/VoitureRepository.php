<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Voiture;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnee;

class VoitureRepository extends AbstractRepository
{

    protected function getNomClePrimaire(): string
    {
        return"immatriculation";
    }


    public function construireDepuisTableau(array $voitureFormatTableau): Voiture
    {
        return new Voiture($voitureFormatTableau['immatriculation'], $voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['nbSieges']);

    }

    protected function getNomTable(): string
    {
        return "voiture";
    }

    public function getNomsColonnes(): array
    {
        return ["immatriculation","marque","couleur","nbSieges"];
    }


}