<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\Repository\VoitureRepository;
use http\Env\Request;

abstract class AbstractControleur{

    public static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "pagetitle" => "Erreur", "cheminVueBody" => "erreur.php"]);
    }

    public static function formulairePreference(){
        $preference=ControleurPreference::lire();
        self::afficherVue('vueGenerale.php',["pagetitle"=>"Formulaire de Préférence","cheminVueBody"=>"formulairePreference.php","preference"=>$preference]);
    }

    public static function enregistrerPreference():void{
        $valeur=$_POST
['controleur_defaut'];
        ControleurPreference::enregistrer($valeur);
        MessageFlash::ajouter("success","La préférence de controleur est enregistrée !");
        header("Location: ../web/controleurFrontal.php");
        exit();
    }




}