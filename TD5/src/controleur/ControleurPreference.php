<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\HTTP\Cookie;

class ControleurPreference
{
    private static string $clePreference = "ControleurPreference";

    public static function enregistrer(string $preference) : void
    {
        Cookie::enregistrer(ControleurPreference::$clePreference, $preference);
    }

    public static function lire() : string
    {
        // À compléter
        if (self::existe())
        return Cookie::lire(ControleurPreference::$clePreference);
        return "";
    }

    public static function existe() : bool
    {
        // À compléter
        return Cookie::contient(ControleurPreference::$clePreference);
    }

    public static function supprimer() : void
    {
        // À compléter
        Cookie::supprimer(ControleurPreference::$clePreference);
    }
}