<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\Repository\VoitureRepository;

class ControleurTrajet extends AbstractControleur
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();     //appel au modèle pour gerer la BD
        AbstractControleur::afficherVue('vueGenerale.php', ["trajets" => $trajets, "pagetitle" => "Liste des trajets", "cheminVueBody" => "trajet/liste.php"]);  //redirige vers la vue
    }
    public static function afficherDetail(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']);
        if (is_null($trajet)) {
            self::afficherErreur("Ce trajet n'existe pas");
        } else {
            AbstractControleur::afficherVue('vueGenerale.php', ["trajet" => $trajet, "pagetitle" => "Detail d'un trajet", "cheminVueBody" => "trajet/detail.php"]);
        }
    }

    public static function supprimer():void{
        $id=$_REQUEST['id'] ;
        (new TrajetRepository())->supprimer($_REQUEST['id']);
        $trajets=(new TrajetRepository())->recuperer();
        AbstractControleur::afficherVue('vueGenerale.php', ["pagetitle" => "Trajet supprimé", "cheminVueBody" => "trajet/trajetSupprimee.php", "trajets" => $trajets, "id" => $id]);
    }

    public static function afficherFormulaireCreation() :void
    {
        AbstractControleur::afficherVue('VueGenerale.php', ["pagetitle" => "Formulaire de création de trajet", "cheminVueBody" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = (new TrajetRepository())->construireDepuisTableau($_REQUEST);
        (new TrajetRepository())->sauvegarder($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        AbstractControleur::afficherVue('vueGenerale.php', ["pagetitle" => "Trajet créé", "cheminVueBody" => "trajet/trajetCree.php", "trajets" => $trajets]);
    }
    public static function afficherFormulaireMiseAJour():void {
        $trajet=(new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']);
        AbstractControleur::afficherVue('vueGenerale.php', ["pagetitle" => "formulaire de mise a jour", "cheminVueBody" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
    }

    public static function mettreAJour():void{
            $trajet=((new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']));
            (new TrajetRepository())->mettreAJour($trajet);
            $id=$_REQUEST['id'];
            AbstractControleur::afficherVue('vueGenerale.php', ["pagetitle" => "trajet Mis a Jour", "cheminVueBody" => "trajet/trajetMisAJour.php", "id" => $id, "trajets" => (new TrajetRepository())->recuperer()]);
    }

    protected function getNomRepository(): string
    {
        return "Trajet";
    }

}