<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\utilisateurRepository;
use App\Covoiturage\Modele\Repository\VoitureRepository;

class ControleurUtilisateur extends AbstractControleur
{
    public static function afficherListe(): void
    {
        $users = (new UtilisateurRepository())->recuperer();     //appel au modèle pour gerer la BD
        AbstractControleur::afficherVue('vueGenerale.php', ["utilisateurs" => $users, "pagetitle" => "Liste des utilisateurs", "cheminVueBody" => "utilisateur/liste.php"]);  //redirige vers la vue
    }

    public static function afficherDetail(): void
    {
        $user = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($user)) {
            self::afficherVue('vueGenerale.php', ["pagetitle" => "Page d'erreur", "cheminVueBody" => "voiture/erreur.php", "messageErreur" => "Cet utilisateur n'existe pas"]);
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $user, "pagetitle" => "Detail d'un utilisateur", "cheminVueBody" => "utilisateur/detail.php"]);
        }
    }

    public static function supprimer():void{
        //TODO redirections flash comme pour mettre a jour
        $login=$_REQUEST['login'] ;
        (new UtilisateurRepository())->supprimer($_REQUEST['login']);
        self::redirectionFlash("success","L'utilisateur a été supprimé","deconnecter");
    }

    public static function afficherFormulaireCreation() :void
    {
     self::afficherVue('VueGenerale.php',["pagetitle"=>"Formulaire de création d'utilisateur","cheminVueBody"=>"utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire(): void
    {
        if (!ConnexionUtilisateur::estAdministrateur()){
            $_REQUEST["estAdmin"]="";
        }
        if ($_REQUEST["mdp"]==$_REQUEST["mdp2"]){
            $user = Utilisateur::construireDepuisFormulaire($_REQUEST);
            (new utilisateurRepository())->sauvegarder($user);
            VerificationEmail::envoiEmailValidation($user);
            MessageFlash::ajouter("success","L'utilisateur a été créé");
            self::afficherListe();
        }else {
            self::afficherFormulaireCreation();
            MessageFlash::ajouter("warning","Mots de passe distincts");
        }

    }

    public static function afficherFormulaireMiseAJour():void{
        $user=(new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($user)){
            MessageFlash::ajouter("warning","L'utilisateur n'existe pas");
            self::afficherListe();
        }else {
            if (ConnexionUtilisateur::estUtilisateur($_REQUEST["login"]) || ConnexionUtilisateur::estAdministrateur()){
                self::afficherVue('vueGenerale.php',["pagetitle"=>"formulaire de mise a jour","cheminVueBody"=>"utilisateur/formulaireMiseAJour.php","user"=>$user]);
            }else {
                MessageFlash::ajouter("danger","Vous ne pouvez pas modifier cet utilisateur");
                self::afficherListe();
            }
        }
    }

    public static function mettreAJour():void{
        //TODO le td demande de rajouter le setEstAdmin mais ça marche sans
        if (!ConnexionUtilisateur::estAdministrateur()){
            $_REQUEST["estAdmin"]="";
        }
        if (isset($_REQUEST["login"],$_REQUEST["prenom"],$_REQUEST["nom"],$_REQUEST["AncienMdp"],$_REQUEST["mdp"],$_REQUEST["mdp2"])) {
            if (ConnexionUtilisateur::estUtilisateur($_REQUEST["login"])) {
                if ($_REQUEST["mdp"] == $_REQUEST["mdp2"]) {
                    $user=(new utilisateurRepository())->recupererParClePrimaire($_REQUEST["login"]);
                    if (!is_null($user)) {
                        if ($user->formatTableau()["mdpHache"] == hash("sha256", $_REQUEST["AncienMdp"])) {
                            $user = Utilisateur::construireDepuisFormulaire($_REQUEST);
                            (new UtilisateurRepository())->mettreAJour($user);
                            self::redirectionFlash("success", "L'utilisateur a été mis à jour", "afficherListe");
                        } else {
                            self::redirectionFlash("warning", "L'ancien mot de passe est érroné", "afficherFormulaireMiseAJour");
                        }
                    }else {
                        self::redirectionFlash("warning","cet utilisateur n'existe pas","afficherFormulaireMiseAJour");
                    }
                } else {
                    self::redirectionFlash("warning", "Mots de passe distincts", "afficherFormulaireMiseAJour");
                }
            }
        else {
                self::redirectionFlash("danger","Vous ne pouvez pas mettre à jour cet utilisateur","afficherListe");
            }
        }else self::redirectionFlash("danger","Impossible de mettre a jour","afficherListe");
    }

    protected function getNomRepository(): string
    {
        return "Utilisateur";
    }
    public static function formulaireConnexion() :void
    {
        self::afficherVue('vueGenerale.php',["pagetitle"=>"formulaire de connexion","cheminVueBody"=>"utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(){
            if (isset($_REQUEST["login"]) && isset($_REQUEST["mdp"])) {
                $user = (new utilisateurRepository())->recupererParClePrimaire($_REQUEST["login"]);
                if (!is_null($user)) {
                    if (VerificationEmail::aValideEmail($user)) {
                        if ($user->formatTableau()["mdpHache"] == hash("sha256", $_REQUEST["mdp"])) {
                            ConnexionUtilisateur::connecter($_REQUEST["login"]);
                            MessageFlash::ajouter("success", "Connexion réussi");
                            self::afficherListe();
                            exit();
                        }else {
                            self::redirectionFlash("warning", "Login ou mot de passe incorrect", "formulaireConnexion");
                        }
                    } else {
                        self::redirectionFlash("warning","L'utilisateur n'a pas valider son email","formulaireConnexion");
                    }
                }
            } else {
                self::redirectionFlash("danger", "Impossible de se connecter", "formulaireConnexion");
            }
    }
    public static function deconnecter(){
        ConnexionUtilisateur::deconnecter();
        self::redirectionFlash("success","Déconnexion réussie","afficherListe");
    }

    public static function redirectionFlash(string $type,string $message,string $action){
        MessageFlash::ajouter($type,$message);
        self::$action();
    }
    public static function validerEmail():void{
        if (isset($_REQUEST["login"], $_REQUEST["nonce"])){
            $bool=VerificationEmail::traiterEmailValidation($_REQUEST["login"],$_REQUEST["nonce"]);
            if ($bool){
                self::redirectionFlash("success","Email Valider","afficherDetail");
            }else {
                self::redirectionFlash("warning","Email non Valider","afficherListe");
            }
        }else{
            self::redirectionFlash("warning","Login ou nonce manquant","afficherListe");
        }
    }
}

?>