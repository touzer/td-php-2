<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\VoitureRepository;

class ControleurVoiture extends AbstractControleur
{
    public static function afficherListe(): void
    {
        $voitures = (new VoitureRepository())->recuperer();     //appel au modèle pour gerer la BD
        AbstractControleur::afficherVue('vueGenerale.php', ["voitures" => $voitures, "pagetitle" => "Liste des voitures", "cheminVueBody" => "voiture/liste.php"]);  //redirige vers la vue
    }

    public static function afficherDetail(): void
    {
        $voiture = (new VoitureRepository())->recupererParClePrimaire($_REQUEST['immatriculation']);
        if (is_null($voiture)) {
            self::afficherVue('vueGenerale.php', ["pagetitle" => "Page d'erreur", "cheminVueBody" => "erreur.php", "messageErreur" => "Cette voiturne n'existe pas"]);
        } else {
            self::afficherVue('vueGenerale.php', ["voiture" => $voiture, "pagetitle" => "Detail d'une voiture", "cheminVueBody" => "voiture/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["pagetitle" => "Formulaire de creation", "cheminVueBody" => "voiture/formulaireCreation.php"]);
    }

    public static function afficherFormulaireMiseAJour():void
    {
        $voiture=(new VoitureRepository())->recupererParClePrimaire($_REQUEST['immatriculation']);
        self::afficherVue('VueGenerale.php',["pagetitle"=>"Formulaire de Mise a Jour","cheminVueBody"=>"voiture/formulaireMiseAJour.php","voiture"=>$voiture]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $voiture = (new VoitureRepository())->construireDepuisTableau($_REQUEST);
        (new VoitureRepository())->sauvegarder($voiture);
        MessageFlash::ajouter("sucess","La voiture a bien été créée");
        self::afficherListe();
    }

    public static function mettreAJour():void
    {
        $voiture=(new VoitureRepository())->recupererParClePrimaire($_REQUEST['immatriculation']);
        (new VoitureRepository())->mettreAJour($voiture);
        MessageFlash::ajouter("success","La voiture a bien été mise à jour");
        self::afficherListe();
    }

    public static function supprimer(): void
    {
        $immat=$_REQUEST['immatriculation'] ;
        (new VoitureRepository())->supprimer($_REQUEST['immatriculation']);
        MessageFlash::ajouter("sucess","La voiture a bien été supprimée");
        self::afficherListe();
    }



}

?>