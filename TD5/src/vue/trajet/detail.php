<?php
$idHTML=htmlspecialchars($trajet->getId());
$departHTML=htmlspecialchars($trajet->getDepart());
$arriveeHTML=htmlspecialchars($trajet->getarrivee());
$dateHTML=htmlspecialchars($trajet->getDate());
$prixHTML=htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML=htmlspecialchars($trajet->getConducteurLogin());
$conducteurLoginURL=rawurlencode($trajet->getConducteurLogin());
$nbPlacesHTML=htmlspecialchars($trajet->getNbPlaces());
echo 'Le trajet '.$idHTML. ' allant de '.$departHTML.' à '.$arriveeHTML.' le '.$dateHTML.' est conduit par
 <a href="../web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login='.$conducteurLoginURL.'">'.$conducteurLoginHTML.'</a> .<br>';
echo 'Il reste '.$nbPlacesHTML.' places et coute '.$prixHTML.' euros.';
