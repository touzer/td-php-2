<!DOCTYPE html>
<html>


<body>


<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="id_id">Id</label> :
            <input type="number" name="id" id="id_id" required>
            <br>
            <label for="depart_id">Depart</label> :
            <input type="text" placeholder="Montpellier" name="depart" id="depart_id" required>
            <br>
            <label for="arrivee_id">Arrivee</label> :
            <input type="text" placeholder="New York" name="arrivee" id="arrivee_id" required>
            <br>
            <label for="date_id">Date</label> :
            <input type="date" placeholder="01/01/2000" name="date" id="date_id " required>
            <br>
            <label for="nbPlaces_id">Nombre de Places</label> :
            <input type="number" placeholder="3" name="nbPlaces" id="nbPlaces_id" required>
            <br>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="50" name="prix" id="prix_id" required>
            <br>
            <label for="conducteurLogin_id">Login du Conducteur</label> :
            <input type="text" placeholder="touzer" name="conducteurLogin" id="conducteurLogin_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer">
            <input type="hidden" name="action" value="creerDepuisFormulaire" />
            <input type="hidden" name="controleur" value="trajet">
        </p>
    </fieldset>
</form>
</body>
</html>