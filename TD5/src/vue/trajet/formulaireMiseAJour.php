<!DOCTYPE html>
<html>


<body>
<?php
$idHTML=htmlspecialchars($trajet->getId());
$departHTML=htmlspecialchars($trajet->getDepart());
$arriveeHTML=htmlspecialchars($trajet->getArrivee());
$dateHTML=htmlspecialchars($trajet->getDate());
$nbPlacesHTML=htmlspecialchars($trajet->getNbPlaces());
$prixHMTL=htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML=htmlspecialchars($trajet->getConducteurLogin());
echo '<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="id_id">id</label> :
            <input type="text" value="' . $idHTML . '" name="id" id="id_id" readonly/>
            <br>
            <label for="depart_id">depart</label> :
            <input type="text" value="' . $departHTML . '" name="depart" id="depart_id" required>
            <br>
            <label for="arrivee_id">arrivee</label> :
            <input type="text" value="' . $arriveeHTML . '" name="arrivee" id="arrivee_id" required>
            <br>
            <label for="date_id">Date</label> :
            <input type="date" value="'.$dateHTML.'" name="date" id="date_id "required>
            <br>
            <label for="nbPlaces_id">Nombre de Places</label>
            <input type="number" value="'.$nbPlacesHTML.'" name="nbPlaces" id="nbPlaces_id" required>
            <br>
            <label for="prix_id">Prix</label>
            <input type="number" value="'.$prixHMTL.'" name="prix" id="prix_id" required>
            <br>
            <label for="conducteurLogin_id">Login du conducteur</label>
            <input type="text" value="'.$conducteurLoginHTML.'" name="conducteurLogin" id="conducteurLogin_id" required>
        </p>
        <p>
            <input type="hidden" name="action" value="mettreAJour" />
            <input type="submit" value="Envoyer">
            <input type="hidden" name="controleur" value="trajet"/>
        </p>
    </fieldset>
</form>'
?>
</body>
</html>