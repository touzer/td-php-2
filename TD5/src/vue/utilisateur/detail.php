<?php
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
$loginHTML=htmlspecialchars($utilisateur->getLogin());
$loginURL=rawurlencode($utilisateur->getLogin());
$nomHTML=htmlspecialchars($utilisateur->getNom());
$prenomHTML=htmlspecialchars($utilisateur->getPrenom());
echo 'L\'utilisateur '.$loginHTML. ' s\'appelle '.$nomHTML.' '.$prenomHTML."<br><br>";
$trajetsConducteur=(new UtilisateurRepository())->getTrajetsParConducteur($utilisateur);
if (is_null($trajetsConducteur)){
    echo "Cet utilisateur n'a conduit aucun trajet";
}else {
    foreach ($trajetsConducteur as $trajet) {
        $trajetHTML=htmlspecialchars($trajet);
        $trajetURL=rawurlencode($trajet);
        echo "Cet utilisateur a conduit le trajet <a href='../web/controleurFrontal.php?action=afficherDetail&controleur=trajet&id=".$trajetURL."'>" . $trajetHTML . "</a>   <br>";
    }
}
if (\App\Covoiturage\Lib\ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || \App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()) {
    echo "<form action='controleurFrontal.php' method='get'>";
    if (\App\Covoiturage\Lib\ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
        echo "<input type='submit' value='supprimer' name='action'/>";
        }
         echo "<input type='submit' value='afficherFormulaireMiseAJour' name='action'/>"
        ."<input type='hidden' value='utilisateur' name='controleur'/>"
        ."<input type='hidden' value='".$loginURL."' name='login'/>"
        ."</form>";
}