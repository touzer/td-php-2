<!DOCTYPE html>
<html>


<body>
<?php
$loginHTML=htmlspecialchars($user->getLogin());
$nomHTML=htmlspecialchars($user->getNom());
$prenomHTML=htmlspecialchars($user->getPrenom());
$bool="";
if (\App\Covoiturage\Lib\ConnexionUtilisateur::utilisateurEstAdmin($user->getLogin())) {
    $bool = "checked";
}
echo '<form method="get" action="../web/controleurFrontal.php" xmlns="http://www.w3.org/1999/html">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label class="InputAddOn-item" for="immat_id">login</label> :
            <input class="InputAddOn-field" type="text" value="' . $loginHTML . '" name="login" id="immat_id" readonly/>
            <br>
            <label class="InputAddOn-item" for="nom_id">nom</label> :
            <input class="InputAddOn-field" type="text" value="' . $nomHTML . '" name="nom" id="nom_id" required>
            <br>
            <label class="InputAddOn-item" for="prenom_id">prenom</label> :
            <input class="InputAddOn-field" type="text" value="' . $prenomHTML . '" name="prenom" id="prenom_id" required>
        </p>
        <p>
            <label class="InputAddOn-item" for="ancienMdp_id">Ancien Mot de Passe</label>
            <input class="InputAddOn-field" type="password" name="AncienMdp" id="ancienMdp_id" required>
            <br>
            <label class="InputAddOn-item" for="mdp_id">Nouveau Mot de Passe</label>
            <input class="InputAddOn-field" type="password" name="mdp" id="mdp_id" required>
            <br>
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau Mot de Passe</label>
            <input class="InputAddOn-field" type="password" name="mdp2" id="mdp2_id" required>
</p>';
if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()){
     echo'   <p class="InputAddOn">
      <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
      <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" '.$bool.'>
    </p>';
}
      echo '  <p>
            <input type="hidden" name="action" value="mettreAJour" />
            <input type="submit" value="Envoyer">
            <input type="hidden" name="controleur" value="utilisateur"/>
        </p>
    </fieldset>
</form>'
?>
</body>
</html>