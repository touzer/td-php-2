<!DOCTYPE html>
<html>
<body>
<?php
$immatriculationHTML=htmlspecialchars($voiture->getImmatriculation());
$marqueHTML=htmlspecialchars($voiture->getMarque());
$couleurHTML=htmlspecialchars($voiture->getCouleur());
$nbSiegesHTML=htmlspecialchars($voiture->getNbSieges());
echo "La voiture ".$immatriculationHTML." est une ".$marqueHTML." ".$couleurHTML." de ".$nbSiegesHTML." places.";
?>
</body>
</html>