<!DOCTYPE html>
<html>


<body>
<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" placeholder="256AB34" name="immatriculation" id="immat_id" required/>
            <br>
            <label for="marque_id">Marque</label> :
            <input type="text" placeholder="Peugeot" name="marque" id="marque_id" required>
            <br>
            <label for="couleur_id">Couleur</label> :
            <input type="text" placeholder="Rouge" name="couleur" id="couleur_id" required>
            <br>
            <label for="nbSieges_id">Nombre de Sieges</label> :
            <input type="number" placeholder="4" name="nbSieges" id="nbSieges_id" required>

        </p>
        <p>
            <input type="submit" value="Envoyer">
            <input type="hidden" name="action" value="creerDepuisFormulaire" />
            <input type="hidden" value="voiture" name="controleur">
        </p>
    </fieldset>
</form>
</body>
</html>