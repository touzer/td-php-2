<!DOCTYPE html>
<html>


<body>
<?php
$immatriculationHTML=htmlspecialchars($voiture->getImmatriculation());
$marqueHTML=htmlspecialchars($voiture->getMarque());
$couleurHTML=htmlspecialchars($voiture->getCouleur());
$nbSiegesHTML=$voiture->getnbSieges();
echo '<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" value="' . $immatriculationHTML . '" name="immatriculation" id="immat_id" readonly/>
            <br>
            <label for="marque_id">Marque</label> :
            <input type="text" value="' . $marqueHTML . '" name="marque" id="marque_id" required>
            <br>
            <label for="couleur_id">Couleur</label> :
            <input type="text" value="' . $couleurHTML . '" name="couleur" id="couleur_id" required>
            <br>
            <label for="nbSieges_id">Nombre de Sieges</label> :
            <input type="number" value="' . $nbSiegesHTML . '" name="nbSieges" id="nbSieges_id" required>

        </p>
        <p>
            <input type="hidden" name="action" value="mettreAJour" />
            <input type="hidden" name="controleur" value="voiture" >
            <input type="submit" value="Envoyer">
        </p>
    </fieldset>
</form>'
?>
</body>
</html>