<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $pagetitle; ?></title>
    <link href="../ressources/css/style.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<header>
    <nav>
        <!-- Votre menu de navigation ici -->
        <ul>
            <?php if (!\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte()) {
               echo ' <li>
                <a href = "controleurFrontal.php?action=formulaireConnexion&controleur=utilisateur" ><img src = "../ressources/images/enter.png" /></a >
            </li >';
            }else {
                $loginURL=rawurlencode(\App\Covoiturage\Lib\ConnexionUtilisateur::getLoginUtilisateurConnecte());
                echo '<li>
                    <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login='.$loginURL.'"><img src="../ressources/images/user.png"/> </a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=deconnecter"><img src="../ressources/images/logout.png"/> </a>
                    </li>';
            }
            ?>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=voiture">Accueil des Voitures</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Accueil des Utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Accueil des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=formulairePreference"><img src="../ressources/images/heart.png"/> </a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    foreach (\App\Covoiturage\Lib\MessageFlash::lireTousMessages() as $type => $lireMessage) {
        echo '<div class="alert alert-'.$type.'">'.$lireMessage.'</div>';
    }
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Voiture
    </p>
</footer>
</body>
</html>
