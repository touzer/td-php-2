<?php

use App\Covoiturage\Controleur\ControleurVoiture as controleurVoiture;

require_once __DIR__. '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
$loader->register();
// enregistrement d'une association "espace de nom" → "dossier"
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');


// On recupère l'action passée dans l'URL
if (!isset($_REQUEST['action'])){
    $action="afficherListe";
}else {
    $action = $_REQUEST['action'];
}
if (isset($_REQUEST['controleur'])){
    $controleur = $_REQUEST['controleur'];
}else if(\App\Covoiturage\Controleur\ControleurPreference::existe()){
    $controleur=\App\Covoiturage\Controleur\ControleurPreference::lire();
} else{
    $controleur="voiture";

}
$nomDeClasseControleur="App\Covoiturage\Controleur\Controleur".ucfirst($controleur);

if (class_exists($nomDeClasseControleur.".php")){
    (new App\Covoiturage\Controleur\ControleurVoiture)->afficherErreur("La classe du controleur n'existe pas");
}else {
    if (in_array($action,get_class_methods($nomDeClasseControleur))){
        $nomDeClasseControleur::$action();
    }else {
        (new App\Covoiturage\Controleur\ControleurVoiture)->afficherErreur("L'action n'existe pas");
    }
}
// Appel de la méthode statique $action de ControleurVoiture



?>